package com.example.mylibrary;

import static com.example.mylibrary.MainActivity.EXTRAS_BOOK;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.webkit.URLUtil;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.mylibrary.common.Utils;
import com.example.mylibrary.dao.BookDao;
import com.example.mylibrary.databaseclient.BookDatabaseClient;
import com.example.mylibrary.databinding.ActivityCreateEditBookBinding;
import com.example.mylibrary.model.Book;
import com.example.mylibrary.viewmodel.BookViewModel;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class CreateEditBookActivity extends AppCompatActivity {
    public static final String TAG = "CreateEditBookActivity";
    public static final String BOOK_DATA = "bookData";
    public static final String EXTRAS_IS_CREATE = "isCreate";

    final int PICK_COVER_REQUEST = 1;

    EditText etCover, etSummary;
    ImageView ivCoverPreview;
    Button btnSubmit, btnOpenFile;

    String title,author,cover,summary;
    int BITMAP_SIZE= 60;
    Bitmap bitmap, decoded;
    Book book;
    BookViewModel bookViewModel;
    boolean isCreate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityCreateEditBookBinding activityCreateEditBookBinding = DataBindingUtil
                .setContentView(this, R.layout.activity_create_edit_book);

//        TODO: UPLOAD IMAGE TO BUCKET (TBD)

        etCover = activityCreateEditBookBinding.etEditCoverUrl;
        etSummary = activityCreateEditBookBinding.etEditSummary;
        ivCoverPreview = activityCreateEditBookBinding.ivEditPreviewCover;
        btnSubmit = activityCreateEditBookBinding.btnEditSubmit;
        btnOpenFile = activityCreateEditBookBinding.btnEditCoverPick;

        etCover.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean focus) {
                if(!focus){
                    BookViewModel.setImageUrl(ivCoverPreview, bookViewModel.getImageLink());
                }
            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onSubmit();
            }
        });

        btnOpenFile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectFile();
            }
        });

        loadExtras();
        activityCreateEditBookBinding.setObj(bookViewModel);
        activityCreateEditBookBinding.executePendingBindings();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == PICK_COVER_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null){
            Uri filePath = data.getData();
            try {
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);

                setToImageView(getResizedBitmap(bitmap, 512));
            }catch (IOException e) {
                e.printStackTrace();
            }
            Toast.makeText(this, "filePath: "+filePath, Toast.LENGTH_SHORT).show();
        }
    }

    void loadExtras(){
        Bundle extras = getIntent().getExtras();
        book = extras.getParcelable(BOOK_DATA);

        if(book == null) {
            Log.d(TAG, "loadExtras: No Book");
            book = new Book();
        }

        isCreate = extras.getBoolean(EXTRAS_IS_CREATE, false);
        if(isCreate) {
            btnSubmit.setText(R.string.create);
        } else {
            btnSubmit.setText(R.string.update);
        }
        bookViewModel = new BookViewModel(this, book);
    }

    void onSubmit() {
        title = String.valueOf(bookViewModel.getTitle());
        author = String.valueOf(bookViewModel.getAuthor());
        cover = String.valueOf(bookViewModel.getImageLink());
        Book book = bookViewModel.getBook();

//        TODO: validate later

        if (!URLUtil.isValidUrl(cover)){
            Toast.makeText(this, "invalid URL", Toast.LENGTH_SHORT).show();
        }else {
            String strData = "title: "+title+", author: "+author;

            Toast.makeText(this, strData, Toast.LENGTH_SHORT).show();
            Log.d(TAG, "coverUrl: "+cover);
        }

        class SubmitBook extends AsyncTask<Void, Void, Void> {

            @Override
            protected Void doInBackground(Void... voids) {
                BookDao bookDao =BookDatabaseClient.getInstance(getApplicationContext())
                        .getBookDatabase()
                        .bookDao();
                if(isCreate){
                    bookDao.insertBook(book);
                    Log.d(TAG, "On Submit: success insert data");
                } else {
                    bookDao.updateBook(book);
                    Log.d(TAG, "On Submit: success update data");
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void unused) {
                super.onPostExecute(unused);

                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                Toast.makeText(getApplicationContext(), "Saved", Toast.LENGTH_SHORT).show();
                startActivity(intent);
                finish();
            }
        }

        SubmitBook sb = new SubmitBook();
        sb.execute();
    }

    void selectFile(){
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);

        startActivityForResult(Intent.createChooser(intent, getString(R.string.select_cover)),
                PICK_COVER_REQUEST);
    }

    void setToImageView(Bitmap bmp) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, BITMAP_SIZE, bytes);
        decoded = BitmapFactory.decodeStream(new ByteArrayInputStream(bytes.toByteArray()));

        ivCoverPreview.setImageBitmap(decoded);
    }

    Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if(bitmapRatio > 1){
            width = maxSize;
            height = (int) (width/ bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }

        return Bitmap.createScaledBitmap(image, width, height, true);
    }
}