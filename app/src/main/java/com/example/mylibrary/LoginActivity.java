package com.example.mylibrary;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.os.Bundle;

import com.example.mylibrary.databinding.ActivityLoginBinding;
import com.example.mylibrary.viewmodel.LoginViewModel;

public class LoginActivity extends AppCompatActivity {
    public static final String TAG = "LoginActivity";

    ActivityLoginBinding activityLoginBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activityLoginBinding = DataBindingUtil.setContentView(this, R.layout.activity_login);
        activityLoginBinding.setLoginViewModel(new LoginViewModel(this));
        activityLoginBinding.executePendingBindings();
    }
}