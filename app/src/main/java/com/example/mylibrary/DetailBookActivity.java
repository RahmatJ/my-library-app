package com.example.mylibrary;

import static com.example.mylibrary.CreateEditBookActivity.BOOK_DATA;
import static com.example.mylibrary.CreateEditBookActivity.EXTRAS_IS_CREATE;
import static com.example.mylibrary.MainActivity.EXTRAS_BOOK;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mylibrary.common.Utils;
import com.example.mylibrary.databaseclient.BookDatabaseClient;
import com.example.mylibrary.databinding.ActivityDetailBookBinding;
import com.example.mylibrary.model.Book;
import com.example.mylibrary.viewmodel.BookViewModel;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

public class DetailBookActivity extends AppCompatActivity {
    public static final String TAG = "DetailBookActivity";

    private Book book;

    Button btnUpdate, btnDelete;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityDetailBookBinding activityDetailBookBinding = DataBindingUtil
                .setContentView(this, R.layout.activity_detail_book);

        btnUpdate = findViewById(R.id.btn_detail_edit);
        btnDelete = findViewById(R.id.btn_detail_delete);

        book = getIntent().getExtras().getParcelable(EXTRAS_BOOK);
        BookViewModel bookViewModel = new BookViewModel(this, book);
        activityDetailBookBinding.setObj(bookViewModel);

        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startUpdateActivity();
            }
        });
        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDeleteDialog(DetailBookActivity.this);
            }
        });
        
        activityDetailBookBinding.executePendingBindings();
    }

    private void showDeleteDialog(Context ctx) {
        AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
        builder.setTitle("Delete this book?");

        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                deleteBook(book);
            }
        });

        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });

        AlertDialog ad = builder.create();
        ad.show();
    }


    private void deleteBook(Book book) {
        class DeleteBook extends AsyncTask<Void, Void, Void> {

            @Override
            protected Void doInBackground(Void... voids) {
                BookDatabaseClient.getInstance(getApplicationContext()).getBookDatabase()
                        .bookDao()
                        .deleteBook(book);

                return null;
            }

            @Override
            protected void onPostExecute(Void unused) {
                super.onPostExecute(unused);

                Toast.makeText(getApplicationContext(), "Deleted", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(DetailBookActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        }

        DeleteBook db = new DeleteBook();
        db.execute();
    }

    void startUpdateActivity(){
        Log.d(TAG, "startUpdateActivity: title: "+book.getTitle());
        Bundle bundle = new Bundle();
        bundle.putParcelable(BOOK_DATA, book);
        bundle.putBoolean(EXTRAS_IS_CREATE, false);

        Intent intent = new Intent(this, CreateEditBookActivity.class);
        intent.putExtras(bundle);
        startActivity(intent);
    }
}