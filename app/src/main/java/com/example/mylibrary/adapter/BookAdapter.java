package com.example.mylibrary.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;

import com.example.mylibrary.BR;
import com.example.mylibrary.R;
import com.example.mylibrary.databinding.ItemRvBookBinding;
import com.example.mylibrary.model.Book;
import com.example.mylibrary.viewmodel.BookViewModel;

import java.util.ArrayList;
import java.util.List;

//TODO: extends baseAdapter
public class BookAdapter extends RecyclerView.Adapter<BookAdapter.BookViewHolder> {

    private List<BookViewModel> bookList;
    private OnBookListener onBookListener;

    public BookAdapter(List<BookViewModel> bookList, OnBookListener onBookListener) {
        this.bookList = bookList;
        this.onBookListener = onBookListener;
    }

    public class BookViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private final ViewDataBinding binding;
        OnBookListener onBookListener;

        public BookViewHolder(ViewDataBinding binding, OnBookListener bookListener) {
            super(binding.getRoot());
            this.binding = binding;
            this.onBookListener = bookListener;

            binding.getRoot().setOnClickListener(this);
        }
        public void bind(Object obj) {
            binding.setVariable(BR.obj, obj);
            binding.executePendingBindings();
        }

        @Override
        public void onClick(View view) {
            onBookListener.onBookClick(getAdapterPosition());
        }
    }

    @Override
    public BookViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        ViewDataBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.item_rv_book,
                parent, false);
        return new BookViewHolder(binding, onBookListener);
    }

    @Override
    public void onBindViewHolder(BookViewHolder holder, int position) {
        final BookViewModel book = bookList.get(position);
        holder.bind(book);
    }

    @Override
    public int getItemCount() {
        return (bookList != null) ? bookList.size() : 0;
    }

    public interface OnBookListener {
        void onBookClick(int position);
    }
}
