//package com.example.mylibrary.adapter;
//
//import android.view.LayoutInflater;
//import android.view.ViewGroup;
//
//import androidx.annotation.NonNull;
//import androidx.databinding.DataBindingUtil;
//import androidx.databinding.ViewDataBinding;
//import androidx.databinding.library.baseAdapters.BR;
//import androidx.recyclerview.widget.RecyclerView;
//
////TODO: Apply this for rv
//public abstract class BaseAdapter extends RecyclerView.Adapter<BaseAdapter.BaseViewHolder> {
//
//    public class BaseViewHolder extends RecyclerView.ViewHolder {
//
//        private final ViewDataBinding binding;
//
//        public BaseViewHolder(ViewDataBinding binding) {
//            super(binding.getRoot());
//            this.binding = binding;
//        }
//        public void bind(Object obj) {
//            binding.setVariable(BR.obj, obj);
//            binding.executePendingBindings();
//        }
//    }
//
//    @Override
//    public void onBindViewHolder(BaseViewHolder holder, int position) {
//        holder.bind(getDataAtPosition(position));
//    }
//
//    @NonNull
//    @Override
//    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
//        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
//        ViewDataBinding binding = DataBindingUtil.inflate(layoutInflater,
//                getLayoutIdForType(viewType), parent, false);
//
//        return new BaseViewHolder(binding);
//    }
//
//    public abstract Object getDataAtPosition(int position);
//    public abstract int getLayoutIdForType(int viewType);
//}
