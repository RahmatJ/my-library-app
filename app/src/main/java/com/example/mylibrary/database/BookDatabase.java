package com.example.mylibrary.database;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.example.mylibrary.dao.BookDao;
import com.example.mylibrary.model.Book;

@Database(entities = Book.class, exportSchema = true, version = 1)
public abstract class BookDatabase extends RoomDatabase {
    public static final String DB_BOOK = "book";

    public abstract BookDao bookDao();
}
