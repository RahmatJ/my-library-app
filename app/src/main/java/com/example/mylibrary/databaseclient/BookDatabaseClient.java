package com.example.mylibrary.databaseclient;

import static com.example.mylibrary.database.BookDatabase.DB_BOOK;

import android.content.Context;

import androidx.room.Room;

import com.example.mylibrary.database.BookDatabase;
import com.example.mylibrary.model.Book;

public class BookDatabaseClient {
    private static BookDatabaseClient mInstance;
    private Context mCtx;
    private BookDatabase bookDatabase;

    public BookDatabaseClient(Context ctx) {
        this.mCtx = ctx;

        bookDatabase = Room.databaseBuilder(mCtx, BookDatabase.class, DB_BOOK).build();
    }

    public static synchronized BookDatabaseClient getInstance(Context ctx) {
        if(mInstance == null) mInstance = new BookDatabaseClient(ctx);

        return mInstance;
    }

    public BookDatabase getBookDatabase() {
        return bookDatabase;
    }
}
