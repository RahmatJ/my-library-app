package com.example.mylibrary.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;
import com.example.mylibrary.model.Book;

import java.util.List;

@Dao
public interface BookDao {
    @Query("SELECT * FROM book")
    List<Book> getAllBook();
    @Insert
    void insertBook(Book book);
    @Delete
    void deleteBook(Book book);
    @Update
    void updateBook(Book book);

}
