package com.example.mylibrary.viewmodel;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;

import com.example.mylibrary.BR;
import com.example.mylibrary.MainActivity;
import com.example.mylibrary.model.Login;

public class LoginViewModel extends BaseObservable {
    public static final String EXTRA_LOGIN_DATA = "ExtraLoginData";
    public static final String TAG = "LoginViewModel";

    private Login login;
    private Context ctx;

    public LoginViewModel(Context ctx) {
        this.login = new Login();
        this.ctx = ctx;
    }

    @Bindable
    public String getUsername() {
        return login.getUsername();
    }

    public void setUsername(String username) {
        login.setUsername(username);
        notifyPropertyChanged(BR.username);
    }

    @Bindable
    public String getPassword() {
        return login.getPassword();
    }

    public void setPassword(String password) {
        login.setPassword(password);
        notifyPropertyChanged(BR.password);
    }

    public Context getCtx() {
        return ctx;
    }

    public void setCtx(Context ctx) {
        this.ctx = ctx;
    }

    public void onLoginClicked() {
        if(isValid()) {
            Log.d(TAG, "username: "+getUsername()+", password: "+getPassword());
            Toast.makeText(ctx, "username: "+getUsername()+", password: "+getPassword(), Toast.LENGTH_SHORT)
                    .show();
            Intent intent = new Intent(ctx, MainActivity.class);
            intent.putExtra(EXTRA_LOGIN_DATA, login);
            ctx.startActivity(intent);
        }else {
            Toast.makeText(ctx, "Password or email invalid", Toast.LENGTH_SHORT).show();
        }
    }

    boolean isValid() {
//        validate login data here

        return !TextUtils.isEmpty(getUsername()) && !TextUtils.isEmpty(getPassword());
    }
}
