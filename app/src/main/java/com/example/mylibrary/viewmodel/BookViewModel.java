package com.example.mylibrary.viewmodel;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.util.Log;
import android.webkit.URLUtil;
import android.widget.ImageView;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;
import androidx.databinding.BindingAdapter;

import com.example.mylibrary.model.Book;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class BookViewModel extends BaseObservable {
    public static final String TAG = "BookViewModel";

    private Book book;
    private Context ctx;

    public BookViewModel(Context ctx, Book book) {
        this.ctx = ctx;
        this.book = book;

        Log.d(TAG, "BookViewModel: Title: "+this.getTitle()+", Author: "+this.getAuthor());
    }

    @Bindable
    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    @Bindable
    public String getTitle() {
        return book.getTitle();
    }

    public void setTitle(String title) {
        book.setTitle(title);
    }

    @Bindable
    public String getAuthor() {
        return book.getAuthor();
    }

    public void setAuthor(String author) {
        book.setAuthor(author);
    }

    @Bindable
    public String getBookId(){
        return String.valueOf(book.getId());
    }

    public void setBookId(String id){
        book.setId(Integer.parseInt(id));
    }

    @Bindable
    public String getImageLink() {
        return book.getImageLink();
    }

    public void setImageLink(String imageLink) {
        book.setImageLink(imageLink);
    }

    public Context getCtx() {
        return ctx;
    }

    public void setCtx(Context ctx) {
        this.ctx = ctx;
    }

    public static List<BookViewModel> bookToViewModel(Context ctx, List<Book> books) {
        List<BookViewModel> mBookViewModels = new ArrayList<BookViewModel>();


        for (Book book: books) {
            Log.d(TAG, "book data: title: "+ book.getTitle() + ", author: "+book.getAuthor());
            BookViewModel bookViewModel = new BookViewModel(ctx, book);
            Log.d(TAG, "book view model: title: "+ bookViewModel.getTitle() +
                    ", author: "+bookViewModel.getAuthor());
            mBookViewModels.add(bookViewModel);
        }

        return mBookViewModels;
    }

    @BindingAdapter({"imageUrl"})
    public static void setImageUrl(ImageView imageView, String url) {
        if(URLUtil.isValidUrl(url)) {
            Picasso.Builder builder = new Picasso.Builder(imageView.getContext()).listener(new Picasso.Listener() {
                @Override
                public void onImageLoadFailed(Picasso picasso, Uri uri, Exception exception) {
                    exception.printStackTrace();
                }
            });
            Picasso picasso = builder.build();
            picasso.load(url).fit().centerInside().into(imageView, new Callback() {
                @Override
                public void onSuccess() {
                    Log.d(TAG, "Success load image!");
                }

                @Override
                public void onError(Exception e) {
                    Log.d(TAG, "Failed load image!");
                }
            });
        }
    }
}
