package com.example.mylibrary;

import static com.example.mylibrary.CreateEditBookActivity.EXTRAS_IS_CREATE;
import static com.example.mylibrary.viewmodel.LoginViewModel.EXTRA_LOGIN_DATA;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.example.mylibrary.adapter.BookAdapter;
import com.example.mylibrary.common.Utils;
import com.example.mylibrary.databaseclient.BookDatabaseClient;
import com.example.mylibrary.databinding.ActivityMainBinding;
import com.example.mylibrary.databinding.ItemRvBookBinding;
import com.example.mylibrary.model.Book;
import com.example.mylibrary.model.Login;
import com.example.mylibrary.viewmodel.BookViewModel;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements BookAdapter.OnBookListener {
    public static final String TAG = "MainActivity";
    public static final String EXTRAS_BOOK = "extrasBook";

    private List<BookViewModel> bookViewModels;
    FloatingActionButton fabCreate;
    RecyclerView rvBook;
    ActivityMainBinding activityMainBinding;
    BookAdapter bookAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);
//        getData
//        getExtras();
        rvBook = activityMainBinding.rvLibrary;
        fabCreate = activityMainBinding.fabCreate;

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        rvBook.setLayoutManager(layoutManager);

        fabCreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) { createBook(); }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

        getBook();
    }

    void getExtras(){
        Login login = getIntent().getParcelableExtra(EXTRA_LOGIN_DATA);
        Toast.makeText(getApplicationContext(),
                "Username: "+ login.getUsername() + ", Password: "+ login.getPassword(),
                Toast.LENGTH_SHORT).show();
    }

    void createBook(){
        Intent intent = new Intent(this, CreateEditBookActivity.class);
        intent.putExtra(EXTRAS_IS_CREATE, true);
        startActivity(intent);
    }

    private void getBook(){
        class GetBook extends AsyncTask<Void, Void, List<Book>> {

            @Override
            protected List<Book> doInBackground(Void... voids) {
                Log.d(TAG, "doInBackground: Getting book ...");
                List<Book> bookList = BookDatabaseClient.getInstance(getApplicationContext())
                        .getBookDatabase().bookDao().getAllBook();
                Log.d(TAG, "doInBackground: Success get book ...");
                return bookList;
            }

            @Override
            protected void onPostExecute(List<Book> books) {
                super.onPostExecute(books);

                bookViewModels = BookViewModel.bookToViewModel(getApplicationContext(),books);
                Log.d(TAG, "getBook: Success set data");
                bookAdapter = new BookAdapter(bookViewModels, MainActivity.this::onBookClick);
                rvBook.setAdapter(bookAdapter);
                activityMainBinding.executePendingBindings();
            }
        }

        GetBook gb = new GetBook();
        gb.execute();

    }

    @Override
    public void onBookClick(int position) {
        Book selectedBook = bookViewModels.get(position).getBook();

        Log.d(TAG, "onBookClick: position: "+position);
        Toast.makeText(this, "Position: "+ position +", Title: "+selectedBook.getTitle(), Toast.LENGTH_SHORT).show();

        Bundle bundle = new Bundle();
        bundle.putParcelable(EXTRAS_BOOK, selectedBook);
        Intent intent = new Intent(this, DetailBookActivity.class);
        intent.putExtras(bundle);
        startActivity(intent);
    }
}