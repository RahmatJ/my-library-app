package com.example.mylibrary.common;

import android.content.Context;
import android.net.Uri;
import android.util.Log;
import android.widget.ImageView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.io.InputStream;

public class Utils {
    public static final String TAG = "Utils";

    public static String getJsonFromAssets(Context context, String fileName) {
        String jsonString;

        try{
            InputStream is = context.getAssets().open(fileName);

            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();

            jsonString = new String(buffer, "UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

        return jsonString;
    }

    public static void setImageFromInternet(Context context, String url, ImageView view){
        Picasso.Builder builder = new Picasso.Builder(context).listener(new Picasso.Listener() {
            @Override
            public void onImageLoadFailed(Picasso picasso, Uri uri, Exception exception) {
                exception.printStackTrace();
            }
        });
        Picasso picasso = builder.build();
        picasso.load(url).fit().centerInside().into(view, new Callback() {
            @Override
            public void onSuccess() {
                Log.d(TAG, "Success load image!");
            }

            @Override
            public void onError(Exception e) {
                Log.d(TAG, "Failed load image!");
            }
        });
    }
}
